<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-outcome
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

class PluginArtefactOutcome extends PluginArtefact {

    public static function get_artefact_types() {
        return array(
            'viewoutcome',
        );
    }
    
    public static function get_block_types() {
        return array();
    }

    public static function get_plugin_name() {
        return 'outcome';
    }

    public static function menu_items() {
        global $USER;
        if (!count_records_sql('
            SELECT COUNT(*) FROM {artefact_outcome_viewoutcomes} ao
            JOIN {view} v ON (v.id = ao.view)
            WHERE v.owner = ?', array($USER->get('id')))) {
            return array();
        }
        return array(
            array(
                'path' => 'myportfolio/outcomes',
                'url' => 'artefact/outcome/',
                'title' => get_string('Outcomes', 'artefact.outcome'),
                'weight' => 40,
            ),
        );
    }

    public static function get_event_subscriptions() {
        return array(
            (object)array('plugin' => 'outcome', 'event' => 'deleteview', 'callfunction' => 'delete_outcomes_for_view'),
        );
    }

    public static function delete_outcomes_for_view($event, $view) {
        if ($ids = get_column('artefact_outcome_viewoutcomes', 'artefact', 'view', $view['id'])) {
            foreach ($ids as $id) {
                $a = artefact_instance_from_id($id);
                $a->delete();
            }
        }
    }

    public static function view_export_extra_artefacts($viewids) {
        if ($viewartefacts = get_records_sql_array("
            SELECT view, artefact
            FROM {artefact_outcome_viewoutcomes}
            WHERE view IN (" . join(',', $viewids) . ')', array())) {
            return $viewartefacts;
        }
        return array();
    }

    public static function view_submit_external_data($viewid) {
        $outcomes = ArtefactTypeViewoutcome::view_outcome_data($viewid);
        foreach ($outcomes as &$o) {
            unset($o->artefact);
            unset($o->author);
            // Save array keys or they get lost on the way
            foreach ($o->scale as $k => $v) {
                $o->scale[$k] = array('name' => $v, 'value' => $k);
            }
        }
        return $outcomes;
    }


    public static function view_release_external_data($view, $outcomes, $teacherid) {
        foreach ($outcomes as $o) {
            if ($oldrecords = get_records_select_array('artefact_outcome_viewoutcomes', 'view = ? AND outcome = ? ', array($view->get('id'), $o['name']))) {
                foreach ($oldrecords as $r) {
                    $artefact = artefact_instance_from_id($r->artefact);
                    $artefact->delete();
                }
            }
            $scale1 = array();
            foreach ($o['scale'] as &$item) {
                $scale1[$item['value']] = $item['name'];
            }
            $data = (object) array(
                'view'    => $view->get('id'),
                'outcome' => $o['name'],
                'scale'   => serialize($scale1),
                'grade'   => $o['grade'],
                'title'   => $o['name'] . ' - ' . $view->get('title'),
                'owner'   => $view->get('owner'),
                'author'  => $teacherid,
            );
            $vo = new ArtefactTypeViewoutcome(0, $data);
            $vo->commit();
        }
    }

}

class ArtefactTypeViewoutcome extends ArtefactType {

    protected $view;    // The graded view
    protected $outcome; // Outcome name from moodle
    protected $scale;   // Outcome scale from moodle
    protected $grade;   // The view's grade on the scale
    protected $author;  // User id of the teacher/marker

    public function __construct($id = 0, $data = null) {
        parent::__construct($id, $data);
        
        if ($this->id && ($outcomedata = get_record('artefact_outcome_viewoutcomes', 'artefact', $this->id))) {
            foreach($outcomedata as $name => $value) {
                if (property_exists($this, $name)) {
                    $this->{$name} = $value;
                }
            }
        }

    }

    public function commit() {
        $new = empty($this->id);

        db_begin();
        parent::commit();

        $data = (object)array(
            'artefact'      => $this->get('id'),
            'view'          => $this->get('view'),
            'outcome'       => $this->get('outcome'),
            'scale'         => $this->get('scale'),
            'grade'         => $this->get('grade'),
            'author'        => $this->get('author'),
        );

        if ($new) {
            insert_record('artefact_outcome_viewoutcomes', $data);
        }
        else {
            update_record('artefact_outcome_viewoutcomes', $data, 'artefact');
        }
        db_commit();

        $this->dirty = false;
    }

    public static function is_singular() {
        return false;
    }

    public static function get_icon($options=null) {
    }

    public function delete() {
        if (empty($this->id)) {
            return; 
        }
        db_begin();
        delete_records('artefact_outcome_viewoutcomes', 'artefact', $this->id);
        parent::delete();
        db_commit();
    }

    public static function get_links($id) {
        return array(
            '_default' => get_config('wwwroot') . 'artefact/outcome/index.php?view=' . $this->get('view'),
        );
    }

    public function render_self() {
        $smarty = smarty_core();
        $smarty->assign('outcome', $this->outcome);
        $scale = unserialize($this->scale);
        $smarty->assign('scale', join(', ', $scale));
        $smarty->assign('grade', $scale[$this->grade]);
        return $smarty->fetch('artefact:outcome:single.tpl');
    }

    public static function user_outcome_data($userid) {
        $records = get_records_sql_array('
            SELECT ao.view, ao.outcome, ao.scale, ao.grade
            FROM {artefact} a JOIN {artefact_outcome_viewoutcomes} ao ON (a.id = ao.artefact)
            WHERE a.owner = ?', array($userid));
        if (empty($records)) {
            return array();
        }
        $data = array();
        foreach ($records as $r) {
            $tempindex = sha1($r->outcome . $r->scale);
            if (!isset($data[$tempindex])) {
                $data[$tempindex] = (object) array(
                    'name'   => $r->outcome,
                    'scale'  => unserialize($r->scale),
                    'grades' => array(),
                );
            }
            $data[$tempindex]->grades[] = (object) array(
                'grade' => $r->grade,
                'view'  => $r->view,
            );
        }
        return array_values($data);
    }

    public static function user_outcome_view_data($userid, $outcomename) {
        $records = get_records_sql_array('
            SELECT ao.view, ao.scale, ao.grade, ao.view, v.title AS viewtitle
            FROM {artefact} a
            JOIN {artefact_outcome_viewoutcomes} ao ON (a.id = ao.artefact)
            JOIN {view} v ON (v.id = ao.view)
            WHERE a.owner = ? AND ao.outcome = ?', array($userid, $outcomename));
        if (empty($records)) {
            return array();
        }
        foreach ($records as &$r) {
            $r->scale = unserialize($r->scale);
        }
        return $records;
    }

    public static function view_outcome_data($viewid) {
        $records = get_records_select_array('artefact_outcome_viewoutcomes', 'view = ?', array($viewid));
        if (empty($records)) {
            return array();
        }
        foreach ($records as &$r) {
            $r->scale = unserialize($r->scale);
        }
        return $records;
    }

    public function get_views_metadata() {
        if (!isset($this->viewsmetadata)) {
            $this->viewsmetadata = array((object) array('artefact' => $this->id, 'view' => $this->view));
        }
        return $this->viewsmetadata;
    }

}


?>

{include file="header.tpl"}

({str tag=aggregatedacrossallviews section=artefact.outcome})
<table id="outcomelist" class="tablerenderer">
 <thead>
  <tr>
   <th>{str tag=Outcome section=artefact.outcome}</th>
   <th>{str tag=evaluatedas section=artefact.outcome}</th>
  </tr>
 </thead>
 <tbody>
 {foreach from=$outcomes item=outcome}
  <tr>
   <td><a href="{$WWWROOT}artefact/outcome/outcome.php?name={$outcome->name|escape:'url'}&user={$user->id}">{$outcome->name|escape}</a></td>
   <td>{$outcome->grade|escape}</td>
  </tr>
 {/foreach}
 </tbody>
</table>

{include file="footer.tpl"}

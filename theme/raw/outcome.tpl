{include file="header.tpl"}

<h3>{$outcomename|escape}</h3>
<table id="outcomelist" class="tablerenderer">
 <thead>
  <tr>
   <th>{str tag=view}</th>
   <th>{str tag=evaluatedas section=artefact.outcome}</th>
  </tr>
 </thead>
 <tbody>
 {foreach from=$outcomes item=outcome}
  <tr>
   <td><a href="{$WWWROOT}artefact/outcome/view.php?view={$outcome->view}">{$outcome->viewtitle|escape}</a></td>
   <td>{$outcome->grade|escape}</td>
  </tr>
 {/foreach}
 </tbody>
</table>

{include file="footer.tpl"}

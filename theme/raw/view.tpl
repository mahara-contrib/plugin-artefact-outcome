{include file="header.tpl"}

<h3>{$viewtitle|escape}</h3>
<table id="outcomelist" class="tablerenderer">
 <thead>
  <tr>
   <th>{str tag=Outcome section=artefact.outcome}</th>
   <th>{str tag=evaluatedas section=artefact.outcome}</th>
  </tr>
 </thead>
 <tbody>
 {foreach from=$outcomes item=outcome}
  <tr>
   <td><a href="{$WWWROOT}artefact/outcome/outcome.php?name={$outcome->outcome|escape:'url'}&user={$ownerid}">{$outcome->outcome|escape}</a></td>
   <td>{$outcome->grade|escape}</td>
  </tr>
 {/foreach}
 </tbody>
</table>

{include file="footer.tpl"}
